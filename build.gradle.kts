import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.8.21"
 //   kotlin("plugin.spring")
 //   id("io.spring.dependency-management")
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    testImplementation(kotlin("test"))
    //implementation("org.springframework.boot:spring-boot-starter:3.1.1")
    implementation("org.springframework.boot:spring-boot-starter-web:3.1.1")
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.apache.curator:curator-x-async:4.0.1") {
        exclude("org.apache.zookeeper:zookeeper")
    }
    implementation("org.apache.zookeeper:zookeeper:3.4.11")
    implementation("com.fasterxml.jackson.core:jackson-databind:2.13.0")
    implementation("org.apache.curator:curator-recipes:4.0.1")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.6.4")
   // implementation( "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.4")
    //implementation( "org.jetbrains.kotlinx:kotlinx-coroutines-core-common")

}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "17"
}
val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "17"
}
val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "17"
}