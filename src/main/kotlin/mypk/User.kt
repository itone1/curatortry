package mypk

import kotlinx.coroutines.*
import org.apache.curator.RetryPolicy
import org.apache.curator.framework.CuratorFramework
import org.apache.curator.framework.CuratorFrameworkFactory
import org.apache.curator.framework.recipes.shared.SharedCount
import org.apache.curator.retry.RetryNTimes
import org.apache.curator.x.async.AsyncCuratorFramework
import org.springframework.stereotype.Component
import java.time.Duration
import java.time.Instant


@Component
class User {
    init {
        runBlocking { doZoo() }
    }
}

class HostConfig(
    hostname: String? = null,
    port: Int = 0, // getters and setters
)

fun main() {
    runBlocking { doZoo() }
}
suspend fun doZoo() {
    val parallel=2
    //val dispatcher = Dispatchers.Default.limitedParallelism(5)
    val dispatcher=Dispatchers.IO.limitedParallelism(parallel)
    val sleepMsBetweenRetries = 100
    val maxRetries = 3
    val retryPolicy: RetryPolicy = RetryNTimes(
        maxRetries, sleepMsBetweenRetries
    )

    val client: CuratorFramework = CuratorFrameworkFactory
        .newClient("127.0.0.1:2181", retryPolicy)
    client.start()
    //val asy = AsyncCuratorFramework.wrap(client)

    val counter = SharedCount(client, "/counters/A", 0)
    counter.start()
    val initValue= counter.count


    var t1 = Instant.now()
    coroutineScope {
        var jobs =MutableList<Job>(parallel){j-> launch(dispatcher) {
            val ch=Char("a".chars().map{i->i+j}.findFirst().asInt)
            val counter = SharedCount(client, "/counters/A", 0)
            counter.start()
            for (i in 1..500) {
                var value = counter.count;
                while (!counter.trySetCount(value + 1)) {
                    println("$ch retry")
                    value = counter.getCount()
                }
                println("$ch ${value+1} ${Thread.currentThread()}")
                //println("$ch" )
                //delay(1);
            }
            counter.close()
        }}
        jobs.joinAll()

    }
    var t2 = Instant.now()
    val finishValue= counter.count

    val seconds =Duration.between(t1,t2).seconds
    val added =finishValue-initValue
    val speed = added/seconds

    println("seconds=$seconds added=$added speed=$speed finishcounter=${counter.count} ")
}